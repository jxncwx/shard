package com.xing.shard.mapper;

import com.xing.shard.entity.LogDate;
import com.xing.shard.interceptor.TableShard;
import com.xing.shard.strategy.TableShardStrategyDate;


import java.util.List;

/**
 * @Author 程序猿阿星
 * @Description
 * @Date 2021/5/8
 */
@TableShard(tableNamePrefix = "tb_log_date",shardStrategy = TableShardStrategyDate.class)
public interface LogDateMapper {

    /**
     * 查询列表-根据日期
     */
    List<LogDate> queryList();

    /**
     * 单插入
     */
    void  save(LogDate logDate);

}
