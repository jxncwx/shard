package com.xing.shard.mapper;


import com.xing.shard.entity.LogId;
import com.xing.shard.interceptor.TableShard;
import com.xing.shard.strategy.TableShardStrategyId;
import org.apache.ibatis.annotations.Param;


/**
 * @Author 程序猿阿星
 * @Description
 * @Date 2021/5/8
 */
@TableShard(tableNamePrefix = "tb_log_id",value = "id",fieldFlag = true,shardStrategy = TableShardStrategyId.class)
public interface LogIdMapper {

    /**
     * 根据id查询-根据id分片
     */
    LogId queryOne(@Param("id") long id);

    /**
     * 单插入-根据id分片
     */
    void save(LogId logId);


}
