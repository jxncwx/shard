package com.xing.shard.interceptor;


import cn.hutool.core.util.StrUtil;

/**
 * @Author 程序猿阿星
 * @Description 分表策略接口
 * @Date 2021/5/9
 */
public interface ITableShardStrategy {


    /**
     * @author: 程序猿阿星
     * @description: 生成分表名
     * @param tableNamePrefix 表前缀名
     * @param value 值
     * @date: 2021/5/9
     * @return: java.lang.String
     */
    String generateTableName(String tableNamePrefix,Object value);

    /**
     * 验证tableNamePrefix
     */
    default void verificationTableNamePrefix(String tableNamePrefix){
        if (StrUtil.isBlank(tableNamePrefix)) {
            throw new RuntimeException("tableNamePrefix is null");
        }
    }
}
